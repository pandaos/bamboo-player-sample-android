package com.pandaos.android.bambooplayersample;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pandaos.bambooplayer.BambooPlayer;
import com.pandaos.pvpclient.models.PvpEntryModel;
import com.pandaos.pvpclient.models.PvpEntryModelCallback;
import com.pandaos.pvpclient.models.PvpEntryModel_;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpMeta;

import java.util.List;

public class MainActivity extends AppCompatActivity implements BambooPlayer.BambooPlayerInterface, PvpEntryModelCallback {

//    Bamboo360Player bamboo_vr_player; //360 video player
    BambooPlayer bamboo_player; //video player

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bamboo_player = (BambooPlayer) findViewById(R.id.bamboo_player);
        bamboo_player.setPlayerInterface(this);
        // play a video
//                bamboo_vr_player.play360Video("https://vp.nyt.com/video/360/hls/video.m3u8");
//        bamboo_vr_player.playEntry("0_24qm7pq1"); //360 entry
//        bamboo_player.playEntry("0_ndied9om"); //regular entry

        PvpEntryModel entryModel = PvpEntryModel_.getInstance_(this);
        entryModel.getEntries(1, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //resume the video when resuming the activity
        bamboo_player.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //pause the video when minimizing or exiting the activity
        bamboo_player.pause();
    }

    @Override
    protected void onDestroy() {
        // stop the player when destroying the activity
        bamboo_player.stop();
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPlayerVideoSizeChanged(int width, int height) {
    }

    @Override
    public void onPlayerStart() {
        System.out.println("player start");
    }

    @Override
    public void onPlayerComplete() {
        System.out.println("player complete");
    }

    @Override
    public void onPlayerError() {
        System.out.println("player error");
    }

    @Override
    public void entryRequestSuccess(PvpEntry entry) {
        
    }

    @Override
    public void entryRequestSuccess(List<PvpEntry> entries) {
        if (entries.size() > 0) {
            bamboo_player.playEntry(entries.get(0));
//            bamboo_player.playEntry(entries.get(0).id);
        }
    }

    @Override
    public void entryRequestSuccessWithMeta(List<PvpEntry> entries, PvpMeta meta) {

    }

    @Override
    public void entryRequestFail() {

    }
}
