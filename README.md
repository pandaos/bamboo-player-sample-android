# Bamboo Video Player Sample Project

This is a sample project that showcases integration of the Bamboo player SDK in an Android app.

Guides for getting the SDK up and running can be found in the [Bamboo Player repository wiki][].

[Bamboo Player repository wiki]: https://bitbucket.org/pandaos/bamboo-player-android/wiki/